#!/bin/bash

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT

start=`date`
exptid=`date +%b%d-%H:%M`

rootdir=initialwindow-$exptid
plotpath=util


for run in 1; do
	dir=$rootdir
	python initCwnd.py --run-rtt 1 --run-bdp 1  --run-bw 1 --run-size 1 --dir $dir

done

echo "Started at" $start
echo "Ended at" `date`
