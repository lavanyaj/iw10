#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

import signal
import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

import sys
import os
from util.monitor import monitor_qlen
from util.helper import stdev

import numpy as np
import matplotlib.pyplot as plt


# Parse arguments
##################

parser = ArgumentParser(description="Initial Congestion Window tests")

# common parameters for graphs
# BW_HOST = 1.2, BW_NET = 1000, SIZE = 35, RTT = 70, N = 2, ORIGWND = 3, NEWWND = 10

parser.add_argument('--plot-only', '-Y',
                    action='store_true',
                    help="plot only (false by default)",
                    default=False)


parser.add_argument('--loss-host', '-V',
                    type=float,
                    help="Loss rate of host link (%)",
                    default=0)

parser.add_argument('--bw-host', '-B',
                    type=float,
                    help="Default Bandwidth of host link (Mb/s)",
                    default=1.2)

parser.add_argument('--bw-net', '-N',
                    type=float,
                    help="Default Bandwidth of server (Mb/s)",
                    default=1000)

parser.add_argument('--size', '-S',
                    type=int,
                    help="Default Size of File to fetch (Segments)",
                    default=35)

parser.add_argument('--rtt', '-R',
                    type=float,
                    help="Default RTT server to host (ms)",
                    default=70)

parser.add_argument('--origWnd', '-O',
                    type=int,
                    help="Original Congestion Window (Segments)",
                    default=3)

parser.add_argument('--newWnd', '-I',
                    type=int,
                    help="Increased Congestion Window (Segments)",
                    default=10)

parser.add_argument('--nsamples', '-L',
                    type=int,
                    help="Number of Samples for avg. Latency",
                    default=30)

parser.add_argument('--run-rtt', '-T',
                    type=int,
                    help="1 to run rtt tests, 0 otherwise",
                    default=0)

parser.add_argument('--rtts', '-t',
                    type=int,
                    nargs = '+',
                    help="RTTs to test",
                    default=[20, 50, 100, 200, 500, 1000, 3000])

parser.add_argument('--run-bw', '-W',
                    type=int,
                    help="1 to run bw_host tests, 0 otherwise",
                    default=0)

parser.add_argument('--bws', '-w',
                    type=int,
                    nargs='+',
                    help="bw_hosts to test",
                    default=[0.056, 0.256, 0.512, 1, 2, 3, 5])


parser.add_argument('--run-bdp', '-P',
                    type=int,
                    help="1 to run bdp tests, 0 otherwise",
                    default=0)

parser.add_argument('--bdps', '-p',
                    type=str,
                    help="bw, delay tuples for bdp test",
                    default='0.160, 50; 0.400, 100; 0.800, 100; 2, 200; 2, 400')


parser.add_argument('--run-size', '-Z',
                    type=int,
                    help="1 to run size tests, 0 otherwise",
                    default=0)


parser.add_argument('--sizes', '-z',
                    type=int,
                    nargs='+',
                    help="sizes to test",
                    default=[3,4,7,10,15,30,50,60])

parser.add_argument('--dir', '-d',
                    type=str,
                    help="output directory",
                    default=".")

args = parser.parse_args()

# threshold for verify_latency
VERIFY_LATENCY_THRESHOLD = 0.1

# threshold for verify_bandwidth
VERIFY_BANDWIDTH_THRESHOLD = 0.1
 
# verify cwnd for packets from server.IP():80
# which experiments to run

# constants
BYTES_PER_SEGMENT = 1430

# Time to wait between samples, in seconds, as a float.
SAMPLE_PERIOD_SEC = 1.0

# Time to wait for first sample, in seconds, as a float.
SAMPLE_WAIT_SEC = 3.0


lg.setLogLevel('info')





# Topology to be instantiated in Mininet
class StarTopo(Topo):
    "Star topology for Initial Window experiment"

    def __init__(self, n=2, cpu=None, bw_host=None, bw_net=None,
                 delay=None, maxq=None):
        # Add default members to class.
        super(StarTopo, self ).__init__()
        self.n = n
        self.cpu = cpu
        self.bw_host = bw_host
        self.bw_net = bw_net
        self.delay = delay # RTT
        self.maxq = maxq
        self.create_topology()

        

    def create_topology(self):
        # add hosts
        client = self.addHost('h0')
        server = self.addHost('h1')
        switch = self.addSwitch('s0')


        # add links
        linkClientToSwitch = {'bw':self.bw_host, \
                                  'delay':str(float(self.delay)/4.0)+'ms',\
                                  'loss':args.loss_host}
        linkServerToSwitch = {'bw':self.bw_net, \
                                  'delay':str(float(self.delay)/4.0)+'ms'}



        self.addLink(client, switch, **linkClientToSwitch);
        self.addLink(server, switch, **linkServerToSwitch);

        
        lg.info("hosts and links added.\n")

        pass

def plot(outfile, xLabel):
    
    f = open("%s/%s" % (args.dir, outfile), "r")

    xValues = []
    latencyAbsolute = []
    latencyPercent = []

    for line in f:
        words = line.split(",")
        xValues.append(float(words[0]))
        latencyAbsolute.append(float(words[1]))
        latencyPercent.append(float(words[2]))
   

    lg.info("xValues %s\n" % str(xValues))
    lg.info("latencyAbsolute %s\n\n" % str(latencyAbsolute))
    pos = np.arange(len(xValues))
    posLeft = [p-0.25 for p in pos]
    posRight = [p for p in pos]
    width = 0.25

    plt.clf()
    axL = plt.subplot(1,1,1)
    axL.yaxis.tick_left()
    
    axL.yaxis.set_label_position("left")
    plt.bar(posLeft, latencyAbsolute, width, color ='r', log=True)
    axL.set_ylim(bottom=1)
    plt.grid(True)

    plt.ylabel("Absolute improvement (ms)")
    plt.xlim(min(xValues), len(xValues))
    plt.xticks(pos, xValues)
    plt.xlabel(xLabel)

    plt.show()

    axR = axL.twinx()
    axR.yaxis.tick_right()
    axR.yaxis.set_label_position("right")
    plt.grid(True)
    plt.bar(posRight, latencyPercent, width, color = 'b')
    plt.ylabel("Percent improvement (%)")
    plt.xlim(-0.75, len(xValues))
    plt.xticks(pos, xValues)
    plt.xlabel(xLabel)


    plt.show()

    
    plt.savefig(('%s/%s.png'% (args.dir, outfile)), dpi=300)


pass

def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),

def parse_tcpdump_rwnd(net, node):
    octets = node.IP().split(".")
    matchSender = "%s\.%s\.%s\.%s\.[0-9]\+ >" %(octets[0], octets[1], octets[2], octets[3])
    
    print matchSender

def start_server(net):
    server = net.getNodeByName('h1')
    proc = server.popen("iperf -s")
    sleep(1)
    return [proc]

    
def start_webserver(net):
    server = net.getNodeByName('h1')
    proc = server.popen("python http/webserver.py", shell=True)
    sleep(1)
    return [proc]


def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))

    return float(line.split()[9])

def get_rates(iface, nsamples=args.nsamples, period=SAMPLE_PERIOD_SEC,
              wait=SAMPLE_WAIT_SEC):
    """Returns the interface @iface's current utilization in Mb/s.  It
    returns @nsamples samples, and each sample is the average
    utilization measured over @period time.  Before measuring it waits
    for @wait seconds to 'warm up'."""
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    sleep(wait)
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time()
        elapsed = now - last_time

        last_time = now
        # Get rate in Mbps; correct for elapsed time.
        rate = (txbytes - last_txbytes) * 8.0 / 1e6 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append(rate)
        last_txbytes = txbytes
        sys.stdout.flush()
        sleep(period)
    return ret

def avg(s):
    "Compute average of list or string of values"
    if ',' in s:
        lst = [float(f) for f in s.split(',')]
    elif type(s) == str:
        lst = [float(s)]
    elif type(s) == list:
        lst = s
    return sum(lst)/len(lst)

def verify_latency(net, lat):
    client = net.getNodeByName('h0')
    server = net.getNodeByName('h1')
    measured_lat = 0
    result = client.cmd("ping -c 4 -q " + str(server.IP()))
    lines = result.split('\n')
    for line in lines:
        if "rtt min/avg/max/mdev" in line:
            measured_lat = float(line.replace("/"," ").split(" ")[6])
    if abs(measured_lat - lat) > VERIFY_LATENCY_THRESHOLD * lat:
        cprint("Latency Test Failed: Measured %d, Expected %d" % (measured_lat, lat), 'red')
        return 0
        #sys.exit(1)
    else:
        cprint("Latency Test Passed: Measured %d, Expected %d" % (measured_lat, lat), 'green')
        return 1

def verify_bandwidth(net, bw):
    client = net.getNodeByName('h0')
    server = net.getNodeByName('h1')

    procServer = server.popen('iperf -s -p %s ' % (5001))
    procClient = client.popen('iperf -c %s -p %s -t 200 -i 1 -yc -Z bic' % (server.IP(), 5001))

    sleep(15)
    measured_bw = avg(get_rates("s0-eth0"))

    if abs(measured_bw - bw) > VERIFY_BANDWIDTH_THRESHOLD * bw:
        cprint("BW Test Failed: Measured %f, Expected %f" % (measured_bw, bw), 'red')
        ret = 0
        #sys.exit(1)
    else:
        cprint("BW Test Passed: Measured %f, Expected %f" % (measured_bw, bw), 'green')
        ret = 1

    #Don't forget to kill iperf
    procServer.kill()
    procClient.kill()

    sleep(8)
    return ret


def verify_cwnd(net, cwnd):
    server = net.getNodeByName('h1')
    client = net.getNodeByName('h0')


    print "started tcp_probe"
    fetch_page(net, 100, 3)

    stop_tcpprobe()

    lines = open("%s/%s" % (args.dir, "cwnd.txt"), 'r').readlines()
    for line in lines:
        if server.IP() in line:
            split_line = line.split(' ')
            if split_line[1] != server.IP()+":80":
                continue
            measured_cwnd = float(split_line[6])
            if measured_cwnd != cwnd:
                cprint("cwnd Test Failed: Measured %d, Expected %d" % (measured_cwnd, cwnd), 'red')
                return 0
                #sys.exit(1)
            else:
                cprint("cwnd Test Passed: Measured %d, Expected %d" % (measured_cwnd, cwnd), 'green')
                return 1

   
    

def fetch_page(net, pageNumSegments, trials=args.nsamples):
    results = []
    server = net.getNodeByName('h1')
    client = net.getNodeByName('h0')

    filename = "http/%d.html" % (pageNumSegments)

    lg.info("curling page of size %d, %d times\n" % (pageNumSegments, trials))

    for i in range(trials):
        
        proc = client.popen("curl -o /dev/null -s -w %%{time_total} %s/%s" \
                                % (server.IP(), filename),\
                                shell = True, stdout = PIPE)

        stdout, stderr = proc.communicate()
        result = stdout.rstrip()

        results.append(float(result) * 1000)
        lg.info(".")
        #sleep(1)
    lg.info("\n")
    return avg(results)

def make_page(pageNumSegments):
    numBytes = pageNumSegments * BYTES_PER_SEGMENT
    filename = "http/%d.html" % pageNumSegments
    f = open(filename, "wb")
    f.seek(numBytes - 1)
    f.write("\0")
    f.close()
     
def change_wnd(net, wnd, value, host):
    tmpfile = 'tmp.txt'
    host.popen('ip route show > %s/%s' % (args.dir, tmpfile), shell=True).wait();
    f = open("%s/%s" % (args.dir, tmpfile), 'r')
    for line in f:
        entry = line.rstrip()
        break

    f.close()
    
    host.popen('ip route replace %s %s %d' % (entry, wnd, value)).wait()
    host.sendCmd('ip route')
    lg.info("New route is:\n")
    lg.info(host.waitOutput())
    host.popen('ip route flush cache').wait()
    host.cmd('sysctl -w net.ipv4.tcp_no_metrics_save=1')
    host.cmd('sysctl -w net.ipv4.route.flush=1')

    sleep(1)

def start_tcpprobe(outfile="cwnd.txt"):
    os.system("rmmod tcp_probe; modprobe tcp_probe full=1;")
    Popen("cat /proc/net/tcpprobe > %s/%s" % (args.dir, outfile),
          shell=True)
    sleep(1)

def stop_tcpprobe():
    Popen("killall cat", shell=True).wait()

def start_tcpdump(iface="s0-eth1", outfile="tcpdump.txt"):
    cmd = "tcpdump -nnvS -i %s > %s/%s" % (iface, args.dir, outfile)
    Popen(cmd, shell=True)

# actually stop cat
def stop_tcpdump():
    Popen("killall -9 cat; rmmod tcp_probe &>/dev/null;", shell=True).wait()

def cleanup():
    with open(os.devnull, 'wb') as devnull:
        try:
            subprocess.check_call('sudo mn -c', shell=True, stdout=devnull, stderr=subprocess.STDOUT)
        except Exception as e:
            lg.error('bad_command\n'+ str(e))



def improv_for(vary='rtt', rtt=args.rtt, size=args.size, bw_host=args.bw_host, bw_net=args.bw_net, origWnd=args.origWnd, newWnd=args.newWnd, plot_only=False):


    outfile = "%s_rtt=%d_size=%d_bwhost=%.3f_bwnet=%d_origWnd=%d_newWnd=%d.txt_losshost=%d" % (vary, rtt, size, bw_host, bw_net, origWnd, newWnd, args.loss_host)

    if plot_only:
        return outfile

    cleanup()

    topo = StarTopo(n=2, bw_host=bw_host, delay=rtt, bw_net=bw_net)


    f = open("%s/%s"%(args.dir,outfile), 'w')

    topo = StarTopo(n=2, bw_host=bw_host, delay=rtt, bw_net=bw_net)

    if (vary == 'rtt'):
        xValues = args.rtts
    elif  (vary == 'bdp'):
        tuples = args.bdps.split(';')
        xValues = []
        for t in tuples:
            pair = t.split(',')
            lg.info(pair)
            bw = float(pair[0])
            delay = int(pair[1])
            xValues.append(tuple([bw, delay]))
        lg.info(xValues)
    elif (vary == 'bw_host'):
        xValues = args.bws
    elif (vary == 'size'):
        xValues = args.sizes
 
    bw_pass = 0
    latency_pass = 0
    cwnd_pass = 0

    for x in xValues:
        if vary == 'rtt':
            rtt = x
        elif vary == 'size':
            size = x
        elif vary == 'bw_host':
            bw_host = x
        elif vary == 'bdp':
            bw_host = x[0]
            rtt = x[1]

        if vary != 'size':
            for nodeName in ['h0','h1']:
                linkInfo = topo.linkInfo(nodeName,'s0')
                print linkInfo
                if vary in ['rtt', 'bdp']:
                    linkInfo['delay'] = "%.1fms" % (float(rtt)/4)
                if vary in ['bdp', 'bw_host'] and nodeName=='h0':
                    linkInfo['bw']= bw_host
                topo.setlinkInfo(nodeName,'s0',linkInfo) 
        

        net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
        net.start()
        dumpNodeConnections(net.hosts)

        server = net.getNodeByName('h1')
        client = net.getNodeByName('h0')


        if vary in ['bdp', 'bw_host','size']:
            bw_pass += verify_bandwidth(net, bw_host)

        if vary in ['rtt', 'bdp','size']:
            latency_pass += verify_latency(net, rtt)

        change_wnd(net, 'initrwnd', newWnd, client)
        change_wnd(net, 'initcwnd', origWnd, server)

        start_tcpprobe()
        sleep(3)
        proc = start_webserver(net)

        verify_cwnd(net, origWnd)        

        lg.info("fetching page for %s=%s\n" % (vary, str(x)))
        original = fetch_page(net, size)

        lg.info("increasing server cwnd")
        change_wnd(net, 'initcwnd', newWnd, server)
        start_tcpprobe()
        sleep(3)
        cwnd_pass += verify_cwnd(net, newWnd)        

        lg.info("fetching page for %s=%s\n" % (vary, str(x)))
        improved = fetch_page(net, size)

        absolute = float(original) - improved
        percent = (float(absolute)/original)  * 100.0
        val = x
        if (vary == 'bdp'):
            val = (x[0] * x[1] * 1000) / 8
        line = "%f, %2.f, %2.f, %f, %f\n" % (val, absolute, percent, original, improved)
        f.write(line)
        lg.info(line),

        # kill webserver before stopping net? how?
        
        proc[0].kill()
        server.popen("ps aux | grep -e 'webserver.py' | grep -v grep | awk '{print $2}' | xargs -i sudo kill {}", shell=True)
        net.stop()

    lg.info("passed %d of %d bandwidth tests\n" % (bw_pass, len(xValues)))
    lg.info("passed %d of %d latency tests\n" % (latency_pass, len(xValues)))
    f.close()
    cleanup()



    return outfile


def run_size(plot_only=False):
    outfile = improv_for('size', plot_only=plot_only)
    plot(outfile, "Size in segments")
    pass

def run_RTT(plot_only=False):
    outfile = improv_for('rtt', plot_only=plot_only)
    plot(outfile, "RTT (ms)")
    pass

def run_BW(plot_only=False):
    
    outfile = improv_for('bw_host', plot_only=plot_only)
    plot(outfile, "Bandwidth (Mbps)")
    pass

def run_BDP(plot_only=False):
    outfile = improv_for('bdp', plot_only=plot_only)
    plot(outfile, "BDP (KBps * RTT in ms)")
    pass


def main():
    "Create network and run Buffer Sizing experiment"
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
        lg.info("made %s" % args.dir)
    lg.info(os.path.exists(args.dir))
    lg.info(args.dir)

    start = time()

    make_page(args.size)
    for size in args.sizes:
        make_page(size)

    cprint("\n****************************\n* Starting IW10 Experiment *\n****************************\n", 'blue')

    if (args.run_rtt > 0):
        cprint("Starting Delay Experiment", 'blue')
        run_RTT(args.plot_only)

    if (args.run_bw > 0):
        cprint("Starting BW Experiment", 'blue')
        run_BW(args.plot_only)

    if (args.run_bdp > 0): 
        cprint("Starting BDP Experiment", 'blue')
        run_BDP(args.plot_only)

    if (args.run_size > 0):
        cprint("Starting Segment Number Experiment", 'blue')
        run_size(args.plot_only)

    cprint("Experiment Complete", 'blue')

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        cleanup()



