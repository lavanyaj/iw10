#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

import signal
import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

import sys
import os
from util.monitor import monitor_qlen
from util.helper import stdev

import numpy as np
import matplotlib.pyplot as plt


# Parse arguments

args_bw_net = 1000
args_bw_host = 1.2
args_delay = 50
args_n = 2

BYTES_PER_SEGMENT = 1430

# Number of samples to take in get_rates() before returning.
NSAMPLES = 3

# Time to wait between samples, in seconds, as a float.
SAMPLE_PERIOD_SEC = 1.0

# Time to wait for first sample, in seconds, as a float.
SAMPLE_WAIT_SEC = 3.0
#3.0

args_dir = '.'


if not os.path.exists(args_dir):
    os.makedirs(args_dir)

lg.setLogLevel('info')


# Topology to be instantiated in Mininet
class StarTopo(Topo):
    "Star topology for Initial Window experiment"

    def __init__(self, n=2, cpu=None, bw_host=None, bw_net=None,
                 delay=None, maxq=None):
        # Add default members to class.
        super(StarTopo, self ).__init__()
        self.n = n
        self.cpu = cpu
        self.bw_host = bw_host
        self.bw_net = bw_net
        self.delay = delay # RTT, so BDP = 6-7 packets 
        self.maxq = maxq
        self.create_topology()

        

    def create_topology(self):
        # add hosts
        client = self.addHost('h0')
        server = self.addHost('h1')
        switch = self.addSwitch('s0')


        # add links
        linkClientToSwitch = {'bw':self.bw_host, \
                                  'delay':str(float(self.delay)/4.0)+'ms'}
        linkServerToSwitch = {'bw':self.bw_net, \
                                  'delay':str(float(self.delay)/4.0)+'ms'}



        self.addLink(client, switch, **linkClientToSwitch);
        self.addLink(server, switch, **linkServerToSwitch);

        
        print("hosts and links added.")

        pass

def plot(outfile, xLabel):
    
    f = open(outfile, "r")

    xValues = []
    latencyAbsolute = []
    latencyPercent = []

    for line in f:
        words = line.split(",")
        xValues.append(float(words[0]))
        latencyAbsolute.append(float(words[1]))
        latencyPercent.append(float(words[2]))
   

    print("xValues %s" % str(xValues))
    print("latencyAbsolute %s" % str(latencyAbsolute))
    pos = np.arange(len(xValues))
    posLeft = [p-0.25 for p in pos]
    posRight = [p for p in pos]
    width = 0.25

    axL = plt.subplot(1,1,1)
    axL.yaxis.tick_left()
    axL.yaxis.set_label_position("left")
    plt.bar(posLeft, latencyAbsolute, width, color ='r')
    plt.grid(True)
    plt.ylabel("Absolute improvement (ms)")
    plt.xlabel(xLabel)

# not sure how to show labels/ ticks for RTT instead of pos = {0, 1, .., 6}
    axL.set_xticks(xValues, minor=False)
    axL.set_xticklabels(xValues, fontdict=None, minor=False)

    plt.show()

    axR = axL.twinx()#plt.subplot(1,1,1, sharex=axL, frameon=False)
    axR.yaxis.tick_right()
    axR.yaxis.set_label_position("right")
    plt.grid(True)
    plt.bar(posRight, latencyPercent, width, color = 'b')
    plt.ylabel("Percent improvement (%)")
    plt.show()

    
    #ax = plt.axes()
    #ax.set_xticks(pos + width/2.0)
    #ax.set_xticklabels(xValues)

    #ax.set_xlabel(xLabel)
    #ax.set_ylabel('Improvement (ms)')

    #plt.bar(pos, latencyAbsolute, width, color = 'r')
    #plt.show()

    plt.savefig(('%s.png'% outfile), dpi=300)


pass

def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),

def parse_tcpdump_rwnd(net, node):
    octets = node.IP().split(".")
    matchSender = "%s\.%s\.%s\.%s\.[0-9]\+ >" %(octets[0], octets[1], octets[2], octets[3])
    
    print matchSender

def start_server(net):
    server = net.getNodeByName('h1')
    proc = server.popen("iperf -s")
    sleep(1)
    return [proc]

    
def start_webserver(net):
    server = net.getNodeByName('h1')
    proc = server.popen("python http/webserver.py", shell=True)
    sleep(1)
    return [proc]


def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))

    return float(line.split()[9])

def get_rates(iface, nsamples=NSAMPLES, period=SAMPLE_PERIOD_SEC,
              wait=SAMPLE_WAIT_SEC):
    """Returns the interface @iface's current utilization in Mb/s.  It
    returns @nsamples samples, and each sample is the average
    utilization measured over @period time.  Before measuring it waits
    for @wait seconds to 'warm up'."""
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    sleep(wait)
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time()
        elapsed = now - last_time

        last_time = now
        # Get rate in Mbps; correct for elapsed time.
        rate = (txbytes - last_txbytes) * 8.0 / 1e6 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append(rate)
        last_txbytes = txbytes
        sys.stdout.flush()
        sleep(period)
    return ret

def avg(s):
    "Compute average of list or string of values"
    if ',' in s:
        lst = [float(f) for f in s.split(',')]
    elif type(s) == str:
        lst = [float(s)]
    elif type(s) == list:
        lst = s
    return sum(lst)/len(lst)

def verify_latency(net, lat):
    client = net.getNodeByName('h0')
    server = net.getNodeByName('h1')
    measured_lat = 0
    result = client.cmd("ping -c 4 -q " + str(server.IP()))
    lines = result.split('\n')
    for line in lines:
        if "rtt min/avg/max/mdev" in line:
            measured_lat = float(line.replace("/"," ").split(" ")[6])
    if abs(measured_lat - lat) > 0.1 * lat:
        cprint("Latency Test Failed: Measured %d, Expected %d" % (measured_lat, lat), 'red')
        #sys.exit(1)
    else:
        cprint("Latency Test Passed: Measured %d, Expected %d" % (measured_lat, lat), 'green')

def verify_bandwidth(net, bw):
    client = net.getNodeByName('h0')
    server = net.getNodeByName('h1')

    server.popen('iperf -s -p %s ' % (5001), shell=True)
    client.popen('iperf -c %s -p %s -t 200 -i 1 -yc -Z bic' % (server.IP(), 5001))

    sleep(15)
    measured_bw = avg(get_rates("s0-eth0"))

    if abs(measured_bw - bw) > .10:
        cprint("BW Test Failed: Measured %f, Expected %f" % (measured_bw, bw), 'red')
        sys.exit(1)
    else:
        cprint("BW Test Passed: Measured %f, Expected %f" % (measured_bw, bw), 'green')

    #Don't forget to kill iperf
    os.system('killall -9 iperf')

def verify_cwnd(net, cwnd):
    server = net.getNodeByName('h1')
    client = net.getNodeByName('h0')

    print "started tcp_probe"
    fetch_page(net, 100, 3)

    stop_tcpprobe()

    lines = open("tcpprobe.txt", 'r').readlines()
    for line in lines:
        if server.IP() in line:
            split_line = line.split(' ')
            if split_line[1] != server.IP()+":80":
                continue
            measured_cwnd = float(split_line[6])
            if measured_cwnd != cwnd:
                cprint("cwnd Test Failed: Measured %d, Expected %d" % (measured_cwnd, cwnd), 'red')
                sys.exit(1)
            else:
                cprint("cwnd Test Passed: Measured %d, Expected %d" % (measured_cwnd, cwnd), 'green')
                break
   
    

def fetch_page(net, pageNumSegments, trials):
    results = []
    server = net.getNodeByName('h1')
    client = net.getNodeByName('h0')

    filename = "http/%d.html" % (pageNumSegments)

    for i in range(trials):
        
        proc = client.popen("curl -o /dev/null -s -w %%{time_total} %s/%s" \
                                % (server.IP(), filename),\
                                shell = True, stdout = PIPE)

        stdout, stderr = proc.communicate()
        result = stdout.rstrip()
       # print "Result %d: %f" %(i, float(result) * 1000)
        results.append(float(result) * 1000)
        sleep(5)

    return avg(results)

def make_page(pageNumSegments):
    numBytes = pageNumSegments * BYTES_PER_SEGMENT
    filename = "http/%d.html" % pageNumSegments
    f = open(filename, "wb")
    f.seek(numBytes - 1)
    f.write("\0")
    f.close()
     
def change_wnd(net, wnd, value, host):
    tmpfile = 'tmp.txt'
    host.popen('ip route show > %s/%s' % (args_dir, tmpfile), shell=True).wait();
    f = open("%s/%s" % (args_dir, tmpfile), 'r')
    for line in f:
        entry = line.rstrip()
        break

    f.close()
    
    host.popen('ip route replace %s %s %d' % (entry, wnd, value)).wait()
    host.sendCmd('ip route')
    #print "New route is:"
    #print host.waitOutput().rstrip()
    host.popen('ip route flush cache').wait()
    host.cmd('sysctl -w net.ipv4.tcp_no_metrics_save=1')
    host.cmd('sysctl -w net.ipv4.route.flush=1')

    sleep(1)

def start_tcpprobe(outfile="cwnd.txt"):
    os.system("rmmod tcp_probe; modprobe tcp_probe full=1;")
    Popen("cat /proc/net/tcpprobe > %s/%s" % (args_dir, outfile),
          shell=True)
    sleep(1)

def stop_tcpprobe():
    Popen("killall cat", shell=True).wait()

def start_tcpdump(iface="s0-eth1", outfile="tcpdump.txt"):
    cmd = "tcpdump -nnvS -i %s > %s/%s" % (iface, args_dir, outfile)
    Popen(cmd, shell=True)

# actually stop cat
def stop_tcpdump():
    Popen("killall -9 cat; rmmod tcp_probe &>/dev/null;", shell=True).wait()

def improvement_for_sizes(outfile="size.txt", rtt=100):
    # AvgDC median bw 1.2Mbps, 1/3 more than 2Mbps
    # SlowDC median bw 500Kbps

    topo = StarTopo(n=args_n, bw_host=1.2,
                    delay=rtt,
                    bw_net=args_bw_net)

    sizes = [3,4,7,10,15,30,50,60]

    origWnd = 3
    newWnd = 10

    outfile = "size_rtt=%d.txt" % rtt
    f = open(outfile, 'w')

    for size in sizes:
        fetchfile = "http/%d.html" % (size)
        try:
            with open(fetchfile) as g: pass
        except IOError as e:
            print "%s doesn't exist." % fetchfile
            make_page(size)
        
        net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
        net.start()
        dumpNodeConnections(net.hosts)

        server = net.getNodeByName('h1')
        client = net.getNodeByName('h0')

        # verify rtt
        verify_latency(net, rtt)

        # change client's rwnd
        change_wnd(net, 'initrwnd', newWnd, client)
    
        start_webserver(net)
        sleep(2)

        change_wnd(net, 'initcwnd', origWnd, server)
        # verify_cwnd(net, origWnd)
        original = fetch_page(net, size, NSAMPLES)

        # set server cwnd to 10 segments
        change_wnd(net, 'initcwnd', newWnd, server)
        improved = fetch_page(net, size, NSAMPLES)

        absolute = float(original) - improved
        percent = (float(absolute)/original)  * 100.0
        line = "%f, %2.f, %2.f, %f, %f\n" % (size, absolute, percent, original, improved)
        f.write(line)
        print(line),
        net.stop()
        pass
    f.close()
    os.system("sudo mn -c")

    pass

    
def improvement_for_RTTs(outfile="rtt.txt", size=7):

    # AvgDC median bw 1.2Mbps, 1/3 more than 2Mbps
    # SlowDC median bw 500Kbps

    topo = StarTopo(n=args_n, bw_host=1.2,
                    delay=args_delay,
                    bw_net=args_bw_net)


    RTTs = [20, 50, 100, 200, 500, 1000, 3000]

    fetchfile = "http/%d.html" % (size)
    try:
        with open(fetchfile) as f: pass
    except IOError as e:
        print "%s doesn't exist." % fetchfile
        make_page(size)
        
    origWnd = 3
    newWnd = 10

    outfile = "rtt_size=%d.txt" % size
    f = open(outfile, 'w')

    for rtt in RTTs:
        # change rtt, just delay from server to switch
        linkInfo = topo.linkInfo('h1','s0')
        print linkInfo
        linkInfo['delay'] = "%.1fms" % (float(rtt)/4)
        topo.setlinkInfo('h1','s0',linkInfo)
        
        linkInfo = topo.linkInfo('h0','s0')
        linkInfo['delay'] = "%.1fms" % (float(rtt)/4)
        topo.setlinkInfo('h0','s0',linkInfo)
 
        # h1-s0 is delay/4, s0-h0 is delay/4

        print linkInfo

        net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
        net.start()
        dumpNodeConnections(net.hosts)


        server = net.getNodeByName('h1')
        client = net.getNodeByName('h0')

        # verify rtt changed in net
        verify_latency(net, rtt)
        verify_bandwidth(net, args_delay)
        # change client's rwnd
        change_wnd(net, 'initrwnd', newWnd, client)


        # set server cwnd to 3 segments
        change_wnd(net, 'initcwnd', origWnd, server)

        start_webserver(net)
        sleep(2)

        # verify server cwnd changed
        verify_cwnd(net, origWnd)
        
        original = fetch_page(net, size, NSAMPLES)

        # set server cwnd to 10 segments
        change_wnd(net, 'initcwnd', newWnd, server)
        improved = fetch_page(net, size, NSAMPLES)

        absolute = float(original) - improved
        percent = (float(absolute)/original)  * 100.0
        line = "%f, %2.f, %2.f, %f, %f\n" % (rtt, absolute, percent, original, improved)
        f.write(line)
        print(line),

        net.stop()
    f.close()

    os.system("sudo mn -c")
    pass

                                      
def improvement_for_BW(rtt=args_delay, outfile="bw.txt"):

    size = 35
    BWs = [0.056, 0.256, 0.512, 1, 2, 3, 5]
    f = open(outfile, 'w')

    for bw in BWs:
        topo = StarTopo(n=args_n, bw_host=bw,
                    delay=rtt,
                    bw_net=args_bw_net)
        net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
        net.start()

        client = net.getNodeByName('h0')
        server = net.getNodeByName('h1')


        #Verify experiment parameters
        verify_latency(net, args_delay)
        verify_bandwidth(net, bw)

        #Verify that we can change initcwnd
        change_wnd(net, 'initrwnd', 15, client)
        change_wnd(net, 'initcwnd', 15, server)

        start_tcpprobe("tcpprobe.txt")
        sleep(3)
        p = start_webserver(net)


        verify_cwnd(net,15)

        change_wnd(net, 'initcwnd', 3, server)
        sleep(3)
        #Do baseline experiment with initcwnd=3
        base_result = fetch_page(net, size, NSAMPLES)
        
        #Do experiment with initcwnd=10
        change_wnd(net, 'initcwnd', 10, server)
        sleep(3)
        change_result = fetch_page(net, size, NSAMPLES)
        

        absolute = float(base_result) - change_result
        percent = (float(absolute)/base_result)  * 100.0
        line = "%f, %2.f, %2.f, %f, %f\n" % (bw*1000, absolute, percent, base_result, change_result)
        f.write(line)
        cprint(line, 'blue')

        net.stop()
        p[0].kill()
        #server.cmd('kill %python')
        #os.system('killall -9 python http/webserver.py')
        server.popen("ps aux | grep -e 'webserver.py' | grep -v grep | awk '{print $2}' | xargs -i sudo kill {}", shell=True)
    f.close()
    os.system("sudo mn -c")

def improvement_for_BDP(size=35, outfile="bdp.txt"):

    #Since BDP is in bytes, it is a bandwidth in KBps times RTT in ms.
    #In the paper they use BDPs of 1000, 5000, 10000, 50000, and 100000.
    #This translates well to (bandwidth, RTT) pairs of
    #(20,50), (50,100), (100,100), (250,250), and (250,400)
    #We use these below, but give the bandwidths in terms of Mbps. 
    BDPs = [[(0.160, 50), (0.400, 100), (0.800, 100), (2, 200), (2, 400)]]
    for bdp in BDPs:
        topo = StarTopo(n=args_n, bw_host=bdp[0],
                    delay='%sms' % bdp[1],
                    bw_net=args_bw_net)
        net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
        net.start()

        
        client = net.getNodeByName('h0')
        server = net.getNodeByName('h1')


        #Verify experiment parameters
        verify_latency(net, bdp[1])
        verify_bandwidth(net, bdp[0])

        #Verify that we can change initcwnd
        change_wnd(net, 'initrwnd', 15, client)
        change_wnd(net, 'initcwnd', 15, server)

        start_tcpprobe("tcpprobe.txt")
        sleep(3)
        p = start_webserver(net)


        verify_cwnd(net,15)

        change_wnd(net, 'initcwnd', 3, server)
        sleep(3)
        #Do baseline experiment with initcwnd=3
        base_result = fetch_page(net, size, NSAMPLES)
        
        #Do experiment with initcwnd=10
        change_wnd(net, 'initcwnd', 10, server)
        sleep(3)
        change_result = fetch_page(net, size, NSAMPLES)
        

        absolute = float(base_result) - change_result
        percent = (float(absolute)/base_result)  * 100.0
        line = "%f, %2.f, %2.f, %f, %f\n" % ((float(bdp[0])*1000/8)*bdp[1], absolute, percent, base_result, change_result)
        f.write(line)
        cprint(line, 'blue')

        net.stop()
        p[0].kill()

        server.popen("ps aux | grep -e 'webserver.py' | grep -v grep | awk '{print $2}' | xargs -i sudo kill {}", shell=True)
    f.close()
    os.system("sudo mn -c")

def run_size():
    os.system("sudo mn -c")

    RTTs = [20, 50, 100, 200, 500, 1000, 3000] 
    
    for r in RTTs:
        outfile = "size_rtt=%d.txt" % r
        improvement_for_sizes(rtt=r, outfile=outfile)
        plot(outfile, "Size in segments")

def run_RTT():
    os.system("sudo mn -c")

    numSegments = [3,4,7,10,15,30,35,50,60]
    for n in numSegments:
        make_page(n)

    for n in numSegments:
        outfile = "rtt_size=%d.txt" % n
        improvement_for_RTTs(size=n, outfile=outfile)
        outfile = "rtt_size=%d.txt" % n
        plot(outfile, "RTT (ms)")


def run_BW():
    os.system("sudo mn -c")
    RTTs = [20, 50, 100, 200, 500, 1000, 3000] 
    for n in RTTs:
        outfile = "bw_rtt=%d.txt" % n
        improvement_for_RTTs(size=n, outfile=outfile)
        plot(outfile, "Bandwidth (kbps)")

def run_BDP():
    os.system("sudo mn -c")
    numSegments = [3,4,7,10,15,30,35,50,60]
    for n in numSegments:
        make_page(n)

    for n in numSegments:
        outfile = "bw_rtt=%d.txt" % n
        improvement_for_BDP(size=n, outfile=outfile)
        plot(outfile, "BDP (KBps * RTT in ms)")

    
def main():
    "Create network and run Buffer Sizing experiment"

    start = time()

    numSegments = [3,4,7,10,15,30,50,60]
    for size in numSegments:
        make_page(size)

    cprint("****************************\n* Starting IW10 Experiment *\n****************************\n", 'blue')

    cprint("Starting Delay Experiment", 'blue')
    run_RTT()
    cprint("Starting BW Experiment", 'blue')
    run_BW()
    cprint("Starting BDP Experiment", 'blue')
    run_BDP()
    cprint("Starting Segment Number Experiment", 'blue')
    run_size()

    cprint("Experiment Complete", 'blue')

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

